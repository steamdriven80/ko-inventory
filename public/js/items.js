var adjustHeight = function() {
    var newHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) * 0.95 + 'px';

    $(".body").css("height", newHeight);
    $(".item").css("width", $(".items").width() *.98 + 'px');

    window.scrollbars = false;
};
//
//$(document).ready(function() {
//    // left: 37, up: 38, right: 39, down: 40,
//// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
//    var keys = [37, 38, 39, 40];
//
//    function preventDefault(e) {
//        e = e || window.event;
//        if (e.preventDefault)
//            e.preventDefault();
//        e.returnValue = false;
//    }
//
//    function keydown(e) {
//        for (var i = keys.length; i--;) {
//            if (e.keyCode === keys[i]) {
//                preventDefault(e);
//                return;
//            }
//        }
//    }
//
//    function wheel(e) {
//        preventDefault(e);
//    }
//
//    var scroll = 0;
//
//    function doWheel(e) {
//    }
//
//    if (window.addEventListener) {
//        window.addEventListener('DOMMouseScroll', doWheel, false);
//    }
//
//    window.onmousewheel = document.onmousewheel = wheel;
//    document.onkeydown = keydown;
//});

$(document).onresize = $(document).onload = adjustHeight();

function ItemsViewModel() {
    var self = this;

    self.headers  = ko.observableArray([{}]);
    self.itemList = ko.observableArray([{}]);

    self.menuData = ko.observable();

    self.contextData = ko.observable();

    self.queryString = ko.observable();

    self.counts = {};

    self.counts.inService = ko.computed(function() {
        return $(this.itemList).filter(function(item) { return item.status === 'InService'; }).length;
    });

    self.counts.inUse = ko.computed(function() {
        return $(this.itemList).filter(function(item) { return item.status === 'InUse'; }).length;
    });

    self.counts.outOfOrder = ko.computed(function() {
        return $(this.itemList).filter(function(item) { return item.status !== 'InService' && item.status !== 'InUse' && item.status !== 'ServiceSoon'; }).length;
    });

    self.renderContext = function() {
       return '';
    };

    self.doQuery = function() {

    };

    self.getItems = ko.computed(function() {
        if( self.queryString() ) {
            var tmp = $(self.itemList()).filter(function (index,item) {
                try {
                    var regex = RegExp('(?:' +self.queryString() + ')');
                } catch(e) {}
                return (regex.exec(item.model+item.manufacturer)) ? true : false;
            });

            if(!tmp.length)
                return;

            return tmp;
        }

        return self.itemList;
    });

    self.displayHeaders = function() {
        return $(self.headers).filter(function(item) {
            if( item.title == 'more' || item.title == 'statusBubbles')
                return false;
            else
                return true;
        });
    };

    self.render = function(item) {
        return item;
    };

    self.renderBubble = function(item) {
        var status = item.status || '';
        switch(status.toLocaleLowerCase()) {
            case 'inservice':
            case 'inuse':
                color = 'green';
                break;
            case 'inmaintenance':
            case 'needsmaintenance':
                color = 'yellow';
                break;
            case 'error':
            case 'unknown':
            default:
                color = 'red';
                break;
        }

        item.fill = color;
        return color;
    };

    self.toggleDetails = function(item) {
        return function() {
            if (!item || !item.id || item.originalEvent) {
                return '';
            }


            item = item || this;
            console.log('on: ', item);

            var obj = document.querySelector('#item-' + item.id+' .more');

            if (obj.innerText == '+') {
                obj.innerText = '-';
            }
            else {
                obj.innerText = '+';
            }

            obj = $("#item-"+item.id);

            obj.toggleClass('menu-open');
        }
    };

    $.get("js/data.tmp.json", function (data) {
        self.headers(data.headers);
        self.itemList(data.elements);
    });

    return self;
}

ko.applyBindings(new ItemsViewModel());